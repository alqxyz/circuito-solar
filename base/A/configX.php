<?php
session_start();
require_once 'ctrlError.php';
#require_once 'conectarBd.php';
require_once 'funciones.php';
require_once 'recursos.php';
require_once 'phpqrcode/phpqrcode.php';

/*PARAMETROS DE CONTROL ERA PARA NUESTRA APLICACION*/

//el salt es para un metodo de encriptacion
$_salt = 'Tr1bÜ.Pr0-C@1_xXx_';

//1-nombre en el cintillo INSTITUCIONAL
$_frase0='Gobernación Bolivariana<br>';
$_frase1='del Estado Monagas';
//2-nombre en el cintillo INSTITUCIONAL
$_frase2='';
$_frase3='';

//para la redes sociales
$_sitioActual="http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

//echo $_sitioActual;
//es para dar estilo al menu deacuerdo a la posicion de Ctrl_url
//$_archivoactual=basename($_SERVER['PHP_SELF']); archivo neto
$_archivoActual=basename($_SERVER['REQUEST_URI']); //archivo con url

$GLOBALS['archivoActual'] = $_archivoActual;

//logo que aparecera en la entrada principal en la ayuda
$_urlLogo='logo.png';

// Nombre de la carpeta del proyecto
$_ubiSistema="circuitosolar";

// DNS o IP del servidor donde estara tu aplicacion
#$_ipServidor='http://www.monagas.gob.ve';
$_ipServidor='http://localhost';

// Esta es la composicion de ambos para la busqueda espeficica de archivos
$_urlEstatica="$_ipServidor/$_ubiSistema";

// Comportarmientos para el menu
$_rutaActual="http://".$_SERVER['SERVER_NAME']."/$_ubiSistema"; 

/*Title fijo para todas la paginas*/
$_nombreSiste="circuitosolar";

// Meta refresh
$_tiempoRefrescar='33333';

// Nombre de la ciudad
$_nomCiudad='Maturín';

// Nombre completo del sitema y resumido se para que se visualizen el en pie y busqueda SEO
$_keywords='Tribu Pro-Cai, Tribu, Pro-Cai, Programador Caimán, ProCai, Canaima, GNU, Linux, GNU/Linux';
$_nomApliResumido='Tribu Pro-Cai';
$_nomCompletoApli='Programador Caimán';
$_nomResuApli=".::$_nomApliResumido::.";

//descripcion del sistema para q aparesca en el meta description junto con el keywords
$_descripcionApli="($_nomCompletoApli),Creando estándares propios de la programación para así alcanzar NUESTRA SOBERANIA TECNOLOGÍCA";
$_descripcionSiste="$_nomApliResumido: $_descripcionApli";

//desarrollador
$_desarrollo='Desarrollado por la Dirección Sectorial para la Ciencia, Tecnología e Innovación';
$_derAutor='2016 - Algunos Derechos Reservados - Bajo <span style="color:silver; font-weight:bold;" title="Modelo de Programación de la TRIBU PRO-CAI">Akuwamari</span>';


$_izq='width:70%;display:block;';
$_cen='width:0%;display:none;';
$_der='width:30%;display:block;';

//FRAMEWORK JS
$_angular='no';

//PERFILES DE DISEÑO CSS
$_rio='si';
$_rioFlex='no';
$_animate='no';
$_jqueryUi='no';
$_bootstrap='no';
$_materialize='no';

//GRAFICAS
$_highcharts='no';

//FUNCIONALIDAD PARA TABLA Y DISEÑO HOJA COMPLETA 
$_fullPage='no';

if ($_fullPage=='si') {
$_izq='width:100%;';
$_cen='width:100%;';
$_der='width:100%;';
}

//INPUT INTELIGENTES
$_chosen='no';

//GOOGLE MAPS
$_gmaps='si';








//API DE TWITTER
$_tw="si";
$_twCuenta="https://twitter.com/pro_cai";
$_twTimeLineVer="<a class='twitter-timeline' height='400' href=$_twCuenta data-widget-id='702805831390208001'>Tweets</a>";
$_twButton="<a href=$_twCuenta class='twitter-follow-button' data-show-count='true' data-lang='es' data-size='large'>Seguir a @".$_twCuenta."</a>";
$_twTweet="<a class='twitter-share-button' href='intent/tweet?text=Ven%20y%20Inscribete' data-hashtags='Tribu,ProCai,2doDíaCanaima' data-size='large'>Tweet</a>";

//API DE FACEBOOK
$_fb='si';
$_fbGrupo='https://www.facebook.com/groups/tribu.pro.cai/';
$_fbPagina='https://www.facebook.com/TribuProCaiX';
$_fbPaginaVer="<div class='fb-page' data-href=$_fbPagina data-tabs='timeline' data-small-header='true' data-adapt-container-width='true' data-hide-cover='false' data-show-facepile='true'><div class='fb-xfbml-parse-ignore'><blockquote cite=$_fbPagina><a href=$_fbPagina>$_fbPagina</a></blockquote></div></div>";
$_fbButton="<div class='fb-like' data-href=$_sitioActual data-width=$_sitioActual data-layout='button_count' data-action='like' data-show-faces='false' data-share='true'></div>";


/*
<div class="fb-like" data-href="<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>" data-layout="box_count" data-action="like" data-show-faces="true" data-share="true"></div>    
<br>
<div style="margin-left:24px; margin-top:8px;">
<a class="twitter-share-button"
  href="<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">
Tweet
</a>

*/
date_default_timezone_set('America/Caracas');
