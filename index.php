<?php
ini_set("display_errors", "on");
require_once 'base/E/maq/iniHtml.php';
require_once 'base/E/maq/head.php';
require_once 'base/E/maq/iniBody.php';

//azul= #1A74CA
//amarillo=#FFBB07
 ?>
<div id="page">

	<div id="header">
		<a class="navicon" href="#menu-left"> </a>
	</div>
	
	<nav id="menu-left">
		<ul>
			<li class="active"><a href="#somos">Somos</a></li>
			<li><a href="#acerca">Acerca</a></li>
			<li><a href="#operamos">Laborar</a></li>
			<li><a href="#eventos">Eventos</a></li>
			<li><a href="#contacto">Contacto</a></li>
		</ul>
	</nav>
</div>


<div class="header" id="move-top">
	<div class="wrap">
		<div class="header-left">
			<div class="logo">
				<a href="index.html"><img src="base/E/img/recursos/logo.png" title="Circuito Informativo Solar" style="width:64px;" /> </a>
			</div>
		</div>
		
		<div class="header-right">
			<div class="top-nav">
				<ul>
					<li class="active"><a href="#somos">Somos</a></li>
					<li><a href="#acerca">Acerca</a></li>
					<li><a href="#operamos">Laborar</a></li>
					<li><a href="#eventos">Eventos</a></li>
					<li><a href="#contacto">Contacto</a></li>
				</ul>
			</div>
					
			<div class="search">
				<div class="search-box">
					<div id="sb-search" class="sb-search">
						<form action="" method="post" >
							<input class="sb-search-input" placeholder="Buscar" type="search" name="search" id="search">
							<input class="sb-search-submit" type="submit" value="">
							<span class="sb-icon-search"> </span>
						</form>
					</div>
				</div>

				<script src="base/E/js/modernizr.custom.js"></script>
				<script src="base/E/js/classie.js"></script>
				<script src="base/E/js/uisearch.js"></script>
				<script> new UISearch( document.getElementById( 'sb-search' ) ); </script>
			</div>
		</div>

		<div class="clear"> </div>
	</div>
		<div class="clear"> </div>
</div>


<div class="slider">
	<div id="da-slider" class="da-slider">
		<div class="da-slide">
			<label><span> </span></label>
			<h2 style="color:#1A74CA;">Comunicación Liberadora</h2>
			<p style="color:#7c0000; font-weight:bold;">Ciudad Maturín</p>
			<i><a href="#"> </a></i>
			<small><a href="#" style="color: #FFBB07; text-shadow: 1px 1px 1px black;" >Circuito Informativo Solar</a></small>
		</div>
	
		<div class="da-slide">
			<label><span> </span></label>
			<h2 style="color:#1A74CA;">Agencia Oriental de Noticias</h2>
			<p style="color:#6d2db9; font-weight:bold;">Agencia ONV</p>
			<i><a href="#"> </a></i>
			<small><a href="#" style="color: #FFBB07; text-shadow: 1px 1px 1px black;" >Circuito Informativo Solar</a></small>
		</div>

		<div class="da-slide">
			<label><span> </span></label>
			<h2 style="color:#1A74CA;">Tus noticias al día</h2>
			<p style="color:#00005e; font-weight:bold;">Informe 24</p>
			<i><a href="#"> </a></i>
			<small><a href="#" style="color: #FFBB07; text-shadow: 1px 1px 1px black;" >Circuito Informativo Solar</a></small>
		</div>

		<div class="da-slide">
			<label><span> </span></label>
			<h2 style="color:#1A74CA;">Revolución Comunicativa</h2>
			<p style="color:#ff0000; font-weight:bold;">Vanguardia FM</p>
			<i><a href="#"> </a></i>
			<small><a href="#" style="color: #FFBB07; text-shadow: 1px 1px 1px black;" >Circuito Informativo Solar</a></small>
		</div>

		<div class="da-slide">
			<label><span> </span></label>
			<h2 style="color:#1A74CA;">Hablamos a través de tus ideas</h2>
			<p style="color:#986f0c; font-weight:bold;"> 7 emisoras de radiodifusión afiliadas a OMACEM</p>
			<i><a href="#"> </a></i>
			<small><a href="#" style="color: #FFBB07; text-shadow: 1px 1px 1px black;" >Circuito Informativo Solar</a></small>
		</div>

		<nav class="da-arrows">
			<span class="da-arrows-prev"></span>
			<span class="da-arrows-next"></span>
		</nav>
	</div>
			<script type="text/javascript" src="base/E/js/jquery.cslider.js"></script>
			<script type="text/javascript">
				$(function() {
					$('#da-slider').cslider({
						autoplay	: true,
						bgincrement	: 450
					});
				});
			</script>
</div>

			 <script src="base/E/js/hover.zoom.js"></script>
						  <script>
					        $(function() {
					            $('.pink').hoverZoom({
					                overlay: true, // false to turn off (default true)
					                overlayColor: '#51A3C8', // overlay background color
					                overlayOpacity: 0.7, // overlay opacity
					                zoom: 0, // amount to zoom(px)
					                speed: 300 // speed of the hover
					            });
					            
					            
					        }); 
			    		</script>
<div class="works" id="somos">
	<div class="wrap">
		<div class="head">
			<span> </span>
			<h3>¿Qué Somos?</h3>
			<center>
				<div style="border:0px solid gray; max-width:800px; width:80%; text-align:justify; padding-top:8px;">
					Un Circuito Informativo Multiplataforma integrado por medios impresos ( Ciudad Maturín ), portales de noticias 
				<a href="http://ciudadMaturín.info.ve/" target="blank" style="color:#7c0000; font-weight:bold;">Ciudad Maturín</a>, 
				<a href="http://www.agenciaorientaldenoticias.com.ve/" target="blank" style="color:#6d2db9; font-weight:bold;">Agencia ONV</a>,
				<a href="http://www.informe24.com.ve/" target="blank" style="color:#00005e; font-weight:bold;">Informe 24</a>,
				<a href="http://www.vanguardiafm.com.ve/" target="blank" style="color:#ff0000; font-weight:bold;">Vanguardia FM</a> 
					y 7 emisoras de radiodifusión afiliadas a OMACEM.
				</div>
			</center>
		</div>

	
		<div id="small-dialog1" class="mfp-hide">
			<div class="pop_up">
				<h2>Circuito Informativo Solar</h2>
				<p class="para"></p>
			</div>
		</div>
	
		<div class="gallery">
			<div class="container">
				<ul id="filters" class="clearfix">
<li>
	<span class="filter" data-filter="ciudadMaturín">
		Medios Impresos
	</span>
</li>

<li>
	<span class="filter" data-filter="ciudadMaturín agenciaONV informe24">
		Medios Digitales
	</span>
</li>

<li>
	<span class="filter" data-filter="vanguardiaFM cunaviche caribeña conexion apamate">
		Medios Radiales
	</span>
</li>

<li title="Circuito Informativo SOLAR">
	<span class="filter active" data-filter="ciudadMaturín agenciaONV informe24 vanguardiaFM cunaviche caribeña conexion apamate">
		Circuito Solar
		<br>
		<img src="base/E/img/recursos/icon.png" alt="" width="30">
	</span>
</li>
				</ul>
			
				<div id="portfoliolist" style="">
					<div class="portfolio ciudadMaturín mix_all" data-cat="ciudadMaturín" style="display: inline-block; opacity: 1;">
						<div class="portfolio-wrapper">				
							<a class="popup-with-zoom-anim pink" href="#small-dialog1">
								<img src="base/E/img/recursos/p1.jpg" alt="Image 2" style="top: 0px;">
							</a>
						</div>
					</div>				
				
					<div class="portfolio agenciaONV mix_all" data-cat="agenciaONV" style="display: inline-block; opacity: 1;">
						<div class="portfolio-wrapper">			
							<a class="popup-with-zoom-anim pink" href="#small-dialog1">
								<img src="base/E/img/recursos/p2.jpg" alt="Image 2" style="top: 0px;">
							</a>
						</div>
					</div>

					<div class="portfolio informe24 mix_all" data-cat="informe24" style="display: inline-block; opacity: 1;">
						<div class="portfolio-wrapper">						
							<a class="popup-with-zoom-anim pink" href="#small-dialog1">
								<img src="base/E/img/recursos/p3.jpg" alt="Image 2" style="top: 0px;">
							</a>
						</div>
					</div>

					<div class="portfolio vanguardiaFM mix_all" data-cat="vanguardiaFM" style="display: inline-block; opacity: 1;">
						<div class="portfolio-wrapper">			
							<a class="popup-with-zoom-anim pink" href="#small-dialog1">
								<img src="base/E/img/recursos/p4.jpg" alt="Image 2" style="top: 0px;">
							</a>
						</div>
					</div>	
									
					<div class="portfolio cunaviche mix_all" data-cat="cunaviche" style="display: inline-block; opacity: 1;">
						<div class="portfolio-wrapper">
							<a class="popup-with-zoom-anim pink" href="#small-dialog1">
								<img src="base/E/img/recursos/p5.jpg" alt="Image 2" style="top: 0px;">
							</a>
						</div>
					</div>			
						
					<div class="portfolio caribeña mix_all" data-cat="caribeña" style="display: inline-block; opacity: 1;">
						<div class="portfolio-wrapper">			
							<a class="popup-with-zoom-anim pink" href="#small-dialog1">
								<img src="base/E/img/recursos/p6.jpg" alt="Image 2" style="top: 0px;">
							</a>
						</div>
					</div>

					<div class="portfolio conexion mix_all" data-cat="conexion" style="display: inline-block; opacity: 1;">
						<div class="portfolio-wrapper">
							<a class="popup-with-zoom-anim pink" href="#small-dialog1">
								<img src="base/E/img/recursos/p7.jpg" alt="Image 2" style="top: 0px;">
							</a>
						</div>
					</div>

					<div class="portfolio apamate mix_all" data-cat="apamate" style="display: inline-block; opacity: 1;">
						<div class="portfolio-wrapper">			
							<a class="popup-with-zoom-anim pink" href="#small-dialog1">
								<img src="base/E/img/recursos/p8.jpg" alt="Image 2" style="top: 0px;">
							</a>
						</div>
					</div>	
			</div>
		</div>
<div class="clear"> </div>

	<script type="text/javascript" src="base/E/js/jquery.mixitup.min.js"></script>
	<script type="text/javascript">
	$(function () {
		var filterList = {
			init: function () {
				// MixItUp plugin
				// http://mixitup.io
				$('#portfoliolist').mixitup({
					targetSelector: '.portfolio',
					filterSelector: '.filter',	
					effects: ['fade'],
					easing: 'snap',
					// call the hover effect
					onMixEnd: filterList.hoverEffect()
				});				
			},
			hoverEffect: function () {
			}
		};
		// Run the show!
		filterList.init();
	});	
	</script>
					<script src="base/E/js/jquery.magnific-popup.js" type="text/javascript"></script>
					<link href="base/E/css/magnific-popup.css" rel="stylesheet" type="text/css">
							<script>
								$(document).ready(function() {
									$('.popup-with-zoom-anim').magnificPopup({
										type: 'inline',
										fixedContentPos: false,
										fixedBgPos: true,
										overflowY: 'auto',
										closeBtnInside: true,
										preloader: false,
										midClick: true,
										removalDelay: 300,
										mainClass: 'my-mfp-zoom-in'
								});
							});
							</script>
<div class="clear"> </div>
				</div>
</div>
			<div class="works-bg">
				
			</div>
			</div>
			</div>
				</div>
			</div>


			<div class="about" id="acerca">
				<div class="wrap">
					<div class="head">
						<span> </span>
						<h3>Acerca de Nosotros</h3>
					</div>
					<div class="about-grids">
						<div class="about-grid">
							<h3>Misión</h3>
							<p style="text-align: justify;">Difundir informaciones, opiniones y mensajes publicitarios con un criterio altamente profesional y ético, contribuyendo de esta manera a mantener informada a toda la población del oriente venezolano y de todo el país.</p>
						</div>
						<div class="about-grid">
							<h3>Visión</h3>
							<p style="text-align: justify;">Convertirnos en el Circuito Informativo más importante del oriente venezolano, tanto por su cobertura multiplataforma, como por su contenido dirigido a facilitar la toma de decisiones de nuestros lectores, oyentes y seguidores multimedia.</p>
						</div>
						<div class="about-grid">
							<h3>Dónde?</h3>
							<p style="text-align: justify;">Estamos ubicados en el estado Monagas y tenemos conexiones en el resto de los estados orientales (<span style="font-weight: bold;">Anzoátegui, Sucre, Nueva Esparta, Bolívar y Delta Amacuro</span>). </p>
						</div>
						<div class="clear"> </div>
					</div>
					<div class="clear"> </div>
				</div>
			</div>


			<div class="services" id="operamos">
				<div class="wrap">
					<div class="head">
						<span> </span>
						<h3>Trabajando asi</h3>
<div style="text-align:justify;">
	El Circuito Informativo Solar o Circuito Solar opera con una línea editorial común, lo que permite que tanto las noticias, como las informaciones y mensajes publicitarios se repliquen en todos los medios al mismo tiempo, multiplicando su capacidad de difusión, utilzando los recursos y las tecnologías existentes:
</div>
					</div>

<br>
					<div class="services-grids">
						<div class="services-grid">
							<div class="services-grid-left">
								<span class="s-icon1"> </span>
							</div>
							<div class="services-grid-right">
								<h4>WEB</h4>
								<p>Llegamos a todos los públicos a través de un CLICK!.</p>
							</div>
						</div>

						<div class="services-grid">
							<div class="services-grid-left">
								<span class="s-icon3"> </span>
							</div>
							<div class="services-grid-right">
								<h4>MOVIL</h4>
								<p>Expandimos tu información de manera individual y directa. </p>
							</div>
						</div>

						<div class="services-grid">
							<div class="services-grid-left">
								<span class="s-icon2"> </span>
							</div>
							<div class="services-grid-right">
								<h4>ESCRITA</h4>
								<p>Publicamos noticias e informaciones de manera rápida y efectiva.</p>
							</div>
						</div>
						<div class="clear"> </div>
					</div>
				</div>
				<div class="services-bg">
				
				</div>
			</div>


			<div class="purches-it">
				<h5>Te gusta?</h5>
				<a href="#contacto"><b style="font-weight: bold;">Contáctanos</b></a>
			</div>


			<div class="latest-eventos" id="eventos">
				<div class="wrap">
					<div class="head">
						<span> </span>
						<h3>Eventos</h3>
					</div>

					<script type="text/javascript" src="base/E/js/jquery.flexisel.js"></script>
					<script type="text/javascript">
					$(window).load(function() {
					    $("#flexiselDemo3").flexisel({
					        visibleItems: 3,
					        animationSpeed: 1000,
					        autoPlay: true,
					        autoPlaySpeed: 3000,            
					        pauseOnHover: true,
					        enableResponsiveBreakpoints: true,
					        responsiveBreakpoints: { 
					            portrait: { 
					                changePoint:480,
					                visibleItems: 1
					            }, 
					            landscape: { 
					                changePoint:640,
					                visibleItems: 2
					            },
					            tablet: { 
					                changePoint:768,
					                visibleItems: 3
					            }
					        }
					    });
					});
					</script>
					<ul id="flexiselDemo3">
					    <li>
					    	<div class="latest-eventos-grid">
					    		<h3>19/<span>09</span></h3>
					    		<p>.</p>
					    	</div>
					    </li>
					    <li>
					    	<div class="latest-eventos-grid">
					    		<h3>12/<span>09</span></h3>
					    		<p>.</p>
					    	</div>
					    </li>
					    <li>
					    	<div class="latest-eventos-grid">
					    		<h3>23/<span>08</span></h3>
					    		<p>.</p>
					    	</div>
					    </li>
					    <li>
					    	<div class="latest-eventos-grid">
					    		<h3>10/<span>08</span></h3>
					    		<p>.</p>
					    	</div>
					    </li>
					</ul>  
					<div class="clear"> </div>


					<div class="bottom-border">
						<span> </span>
					</div>


				</div>
			</div>


			<div class="contact" id="contacto">
				<div class="wrap">
					<div class="contact-left">
						<div class="contact-head">
								<span> </span>
								<h3>Tu opinión</h3>
						</div>

<form action="base/R/enviarEmail.php" method="POST" >
	<div class="form-left">
		<input type="text" name="_nomApe" placeholder="Nombre y apellido" required >
		<input type="number" name="_num" placeholder="Telefono" required >
		<input type="email" name="_cor" placeholder="Correo" required >
		<div class="clear"> </div>
		<center>
	<input type="submit" value="Contactar" >
		</center>
<div class="clear"> </div>
	</div>

	<div class="form-right">
		<input type="text" name="_asun" placeholder="asunto" required >

<textarea style="resize: none;max-width:1000px;" rows="2" cols="50" onfocus="if(this.value == 'Aporte') this.value='';" onblur="if(this.value == '') this.value='Aporte';" name="_cont" required ></textarea>
	</div>

	
</form>
			</div>


					<div class="contact-right">
						<div class="contact-head">
								<span> </span>
								<h3>Contacto</h3>
						</div>
						<div class="flicker-grids">

<b style="font-weight: bold;">DIRECCIÓN</b>:<br>
Avenida Bolívar, Edificio Lotería de Oriente, Piso 5, zona postal 6201, Maturín, Estado Monagas, Venezuela
<br><br>
<b style="font-weight: bold;">TELÉFONOS</b>:<br>
0291-6421738 | 0426-5757332 
<br>
0416-3960782 | 0416-6730936
<br>
 0426-4969153  
<br><br>
<b style="font-weight: bold;">CORREOS ELECTRÓNICOS</b>:<br>
Publicidad:<br> cir.info.solar@gmail.com 
<!--							<ul>
								<li><a href="#"><img src="base/E/img/recursos/f1.jpg" alt="" /></a></li>
								<li><a href="#"><img src="base/E/img/recursos/f2.jpg" alt="" /></a></li>
								<li><a href="#"><img src="base/E/img/recursos/f3.jpg" alt="" /></a></li>
								<li><a href="#"><img src="base/E/img/recursos/f4.jpg" alt="" /></a></li>
								<li><a href="#"><img src="base/E/img/recursos/f5.jpg" alt="" /></a></li>
								<li><a href="#"><img src="base/E/img/recursos/f6.jpg" alt="" /></a></li>
							</ul>
-->
						</div>
					</div>
					<div class="clear"> </div>
<?php

						if (!empty($_GET['correo'])) {
							
							if ($_GET['correo']=="Enviado") {
							echo "Tu correo se envio correctamente. Pronto nos contactaremos CONTIGO.";
							}
							elseif ($_GET['correo']=="Rechazado") {
							echo "Al parecer hay detalles tecnicos, intenta mas tarde.";
							} 
							else{
							echo "Sigue intentando...";
							}

						}
?>
				</div>
			</div>


			<div class="footer">
				<div class="wrap"> 
				<div class="footer-left">
					<h2>Encuéntranos <span>y comparte con nosotros</span></h2>
				</div>
				<div class="footer-right">
					<ul>
						<li><a class="facebook" href="#" target="blank"><span> </span></a></li>
						<li><a class="twitter" href="https://twitter.com/circuito_solar" target="blank"><span> </span></a></li>
						<li><a class="youtube" href="https://www.youtube.com/channel/UCEPm25wu7gr91M7gaKJvuyA" target="blank"><span> </span></a></li>
						<div class="clear"> </div>
					</ul>
				</div>
				<div class="clear"> </div>
				</div>
			</div>

			<div class="copy-right">
				<div class="wrap">
					<p>©Copyleft 2016 - Algunos derechos reservados.
					<br>
					Hecho Bajo
					<a href="http://www.monagas.gob.ve/tribu/guia.php" title="Control ERA" target="blank">
					<b style="font-weight: bold;">Control ERA</b>
					 </a></p>

					<script type="text/javascript" src="base/E/js/move-top.js"></script>
					<script type="text/javascript">
							$(function() {
							  $('a[href*=#]:not([href=#])').click(function() {
							    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
						
							      var target = $(this.hash);
							      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
							      if (target.length) {
							        $('html,body').animate({
							          scrollTop: target.offset().top
							        }, 1000);
							        return false;
							      }
							    }
							  });
							});
					</script>

					<script type="text/javascript">
							$(document).ready(function() {
								var defaults = {
						  			containerID: 'toTop', // fading element id
									containerHoverID: 'toTopHover', // fading element hover id
									scrollSpeed: 1200,
									easingType: 'linear' 
						 		};
								
								$().UItoTop({ easingType: 'linear' });
								
							});
						</script>
				    <a href="#move-top" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 1;"> </span></a>
				</div>
			</div>

<?php
require_once 'base/E/maq/finBody.php';
require_once 'base/E/maq/finHtml.php';
 ?>